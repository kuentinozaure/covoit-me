import { Component, OnInit, HostListener } from '@angular/core';
import { routes } from 'src/app/config-route';
import { Router } from '@angular/router';
import { PwaService } from 'src/app/shared/services/pwa.service';

@Component({
  selector: 'app-side-nav-bar',
  templateUrl: './side-nav-bar.component.html',
  styleUrls: ['./side-nav-bar.component.css']
})
export class SideNavBarComponent implements OnInit {

  routerConfig = routes;

  isInStandaloneMode: boolean;
  deferredPrompt: any;

  constructor(private router: Router, private pwaService: PwaService) {
   this.isInStandaloneMode = ('standalone' in window.navigator);

  }

  ngOnInit() {}

  changeRoute(choosedUrl: string) {
    this.router.navigate([choosedUrl], {skipLocationChange: true});
  }





  @HostListener('window:beforeinstallprompt', ['$event'])
  onbeforeinstallprompt(e) {
    console.log(e);
    e.preventDefault();
    this.deferredPrompt = e;
    // this.showButton = true;
  }

  installApp() {
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then(choiceResult => {
      if (choiceResult.outcome === 'accepted') {
      } else {
      }
      this.deferredPrompt = null;
    });
  }


}
