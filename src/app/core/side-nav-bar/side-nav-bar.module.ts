import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideNavBarComponent } from './side-nav-bar.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [SideNavBarComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    SideNavBarComponent
  ]
})
export class SideNavBarModule { }
