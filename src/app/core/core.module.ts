import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideNavBarModule } from './side-nav-bar/side-nav-bar.module';
import { SharedModule } from '../shared/shared.module';
import { UpdateComponent } from './update/update.component';


@NgModule({
  declarations: [UpdateComponent],
  imports: [
    CommonModule,
    SideNavBarModule,
    SharedModule,
  ],
  exports: [
    SideNavBarModule,
    UpdateComponent
  ]
})
export class CoreModule { }
