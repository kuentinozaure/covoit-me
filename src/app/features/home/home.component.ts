import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routes } from 'src/app/config-route';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  routerConfig = routes;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToDetail() {
    this.router.navigate([this.routerConfig.detail.path], {skipLocationChange: true});
  }

}
