import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FindJourneyComponent } from './find-journey.component';

const routes: Routes = [{ path: '', component: FindJourneyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class FindJourneyRoutingModule {}
