import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FindJourneyRoutingModule } from './find-journey-routing.module';
import { FindJourneyComponent } from './find-journey.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [FindJourneyComponent],
  imports: [
    CommonModule,
    FindJourneyRoutingModule,
    SharedModule
  ]
})
export class FindJourneyModule { }
