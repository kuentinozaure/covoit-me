import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { DetailJourneyRoutingModule } from './detail-journey-routing.module';
import { DetailJourneyComponent } from './detail-journey.component';



@NgModule({
  declarations: [DetailJourneyComponent],
  imports: [
    CommonModule,
    SharedModule,
    DetailJourneyRoutingModule
  ]
})
export class DetailJourneyModule { }
