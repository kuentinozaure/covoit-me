import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DetailJourneyComponent } from './detail-journey.component';

const routes: Routes = [{ path: '', component: DetailJourneyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)]
})
export class DetailJourneyRoutingModule {}
