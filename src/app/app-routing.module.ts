import { routes } from './config-route';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)
  },
  {
    path: routes.home.path,
    loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule),
  },
  {
    path: routes.detail.path,
    loadChildren: () => import('./features/detail-journey/detail-journey.module').then(m => m.DetailJourneyModule),
  },
  {
    path: routes.find.path,
    loadChildren: () => import('./features/find-journey/find-journey.module').then(m => m.FindJourneyModule),
  },
];
