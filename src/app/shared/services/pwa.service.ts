import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatDialog } from '@angular/material/dialog';
import { UpdateComponent } from 'src/app/core/update/update.component';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PwaService {

  constructor(private swUpdate: SwUpdate, public dialog: MatDialog) {

    const numbers = interval(1000);
    const waitaTime = numbers.pipe(take(5));


    swUpdate.available.subscribe(event => {

      const dialogRef = this.dialog.open(UpdateComponent, {
        width: '250px',
        disableClose: true,
      });

      waitaTime.subscribe(x => {
        if (x === 5) {
          window.location.reload();
        }
      });
    });
  }

}
